FROM busybox:latest
LABEL maintainer https://gitlab.com/tyumin.yuri
WORKDIR build
RUN touch house.txt &&\
    echo "one" >> house.txt &&\
    echo "two" >> house.txt &&\
    ls &&\
    echo "cat house.txt" &&\
    cat house.txt
